module.exports = ( { chalk, shell } ) => {

    return new Promise( (resolve) => {
        console.log(
            chalk.hex('#2affab')(`\nInstalling dependencies, this may take a few minutes...`)
        );

        shell.exec("npm install");

        console.log(
            chalk.hex('#2affab')(`\nDependencies installed.`)
        );

        resolve([
            chalk.hex('#2affab')(`\n    All done. Use the following commands to start working on your project:`),
            chalk.green(`\n\n       vuepress dev or npm run docs:dev\n\n`)
        ]);
    });
};