module.exports = {
    theme: '@fanxie/fanxie',
    dest: 'public',
    title: 'Project docs',
    themeConfig: {
        // logo: '',
        // repo: '',
        nav: [
            // {text: 'Guide', link: '/guide/'}
        ],
        sidebar: [
            '/'
        ],
        // firebase: {
        //     projectSlug: '<-- INTERNAL PROJECT SLUG -->',
        //     protectSite: true,
        //     config: {
        //         apiKey: "<-- API KEY -->",
        //         authDomain: "<-- FIREBASE PROJECT ID -->.firebaseapp.com",
        //         databaseURL: "https://<-- FIREBASE  PROJECT ID -->.firebaseio.com",
        //         projectId: "<-- FIREBASE PROJECT ID -->"
        //     }
        // }
    }
};
